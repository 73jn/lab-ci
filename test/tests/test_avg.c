#include "avg.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{
    /* Set stuff up here */
}

void tearDown(void)
{
    /* Clean stuff up here */
}

void test_avg_function(void)
{
    /* Check result */
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, avg(0, 0), "Error in avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, avg(0, 1), "Error in avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(0, avg(10, -10), "Error in avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(15, avg(20, 10), "Error in avg");
    TEST_ASSERT_EQUAL_INT_MESSAGE(-2, avg(-1, -3), "Error in avg");
    //TEST_ASSERT_EQUAL_INT_MESSAGE(10, avg(1, 1), "Happy Error");
}