#ifndef __AVG_H
#define __AVG_H
#include <stdint.h>
/** @return The sum of two integers @ref a and @ref b. */
int32_t avg(int32_t a, int32_t b);

#endif //__SUM_H